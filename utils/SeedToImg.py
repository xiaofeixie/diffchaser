
from tensorflow import keras
from keras.models import Model
from keras.datasets import mnist
from PIL import Image
from keras.models import load_model
from os import listdir
from os.path import isfile, join
import numpy as np
import os
import cv2

from keras.datasets import mnist
from PIL import Image
from keras.models import load_model
import numpy as np
from keras.datasets import cifar10
import time
import argparse
import struct

import random





def check(queue, imag_path):
    if not os.path.exists(imag_path):
        os.makedirs(imag_path)
    lst = os.listdir(queue)

    for f in lst:
        file = os.path.abspath(join(queue, f))
        if not isfile(file):
            continue

        ori_batch = np.load(file)


        x, m, n, q = ori_batch.shape
        ori_batch = ori_batch[1]

        x = np.reshape( ori_batch, (m,n,q))
        img0 = Image.fromarray(x)
        img0.save( imag_path+'/'+f  + ".png")



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='control experiment')


    parser.add_argument('-o', help='Fuzzing outputs')
    parser.add_argument('-i', help='Input path')
    args = parser.parse_args()
    check(args.i, args.o)
    print('finish')

