import tensorflow as tf
import argparse
import numpy as np
import os
from keras import Input
from keras.models import load_model, save_model

from keras.applications.resnet import ResNet50
from keras.applications.mobilenet import MobileNet
# from PIL import Image

from keras.applications.vgg16 import preprocess_input

os.sys.path.append('../')

shape_dic = {
    'vgg16': (32,32,3),
    'resnet20': (32,32,3),
    'lenet1': (28,28,1),
    'lenet4': (28,28,1),
    'lenet5': (28,28,1),
    'mobilenet': (224, 224,  3),
    'vgg19': (224, 224,  3),
    'resnet50': (224, 224,  3)
}
def color_preprocessing(x_test):
    temp = np.copy(x_test)
    temp = temp.astype('float32')
    mean = [125.307, 122.95, 113.865]
    std = [62.9932, 62.0887, 66.7048]
    for i in range(3):
        temp[:, :, :, i] = (temp[:, :, :, i] - mean[i]) / std[i]
    return temp

def preprocessing_test_batch(x_test):
    temp = np.copy(x_test)
    temp = temp.reshape(temp.shape[0], 28, 28, 1)
    temp = temp.astype('float32')
    temp /= 255
    return temp
def preprocess_image(input_img_data):
    temp = np.copy(input_img_data)
    temp = np.float32(temp)
    qq = preprocess_input(temp)  # final input shape = (1,224,224,3)
    return qq


def transferModel(model, percent):
    weights = model.get_weights()

    w = np.asarray(weights[0])
    for item in weights[1:]:
        item = np.asarray(item)
        w = np.append(w, item)

    weights_size = np.size(w)
    sample_idxs = np.random.choice(weights_size, int(weights_size * percent), replace=False)

    w[sample_idxs] = np.float16(w[sample_idxs])
    cur = 0
    for index, item  in enumerate(weights):
        shape = item.shape
        size = np.size(item)
        sh = w[cur:cur+size]
        cur += size
        weights[index] = np.reshape(sh, shape)

    model.set_weights(weights)
    return model


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='coverage guided fuzzing')

    parser.add_argument('-o', help='')
    parser.add_argument('-i')

    parser.add_argument('-percentage', help='The percentage to truncate weights from FLOAT32 to FLOAT16', default=1.0, type=float)
    parser.add_argument('-model_type', help='Out path', choices=['lenet5', 'resnet20','mobilenet', 'lenet1','vgg16', 'resnet50'], default='lenet5')
    args = parser.parse_args()

    if args.model_type == 'mobilenet':
        img_rows, img_cols = 224, 224
        input_shape = (img_rows, img_cols, 3)
        input_tensor = Input(shape=input_shape)
        # model1 = MobileNet(input_tensor=input_tensor)
        model2 = MobileNet(input_tensor=input_tensor)
    elif args.model_type == 'resnet50':
        img_rows, img_cols = 224, 224
        input_shape = (img_rows, img_cols, 3)
        input_tensor = Input(shape=input_shape)
        model2 = ResNet50(input_tensor=input_tensor)
    else:
        model1= load_model(args.i)
        # model2 = build_model(model_weight_path[args.model_type], args.model_type)

    if not os.path.exists(args.o):
        os.makedirs(args.o)

    # ori_model_path = os.path.join(args.o, args.model_type+'.h5')
    # save_model(model2, ori_model_path)
    percentage = [0.01, 0.5, 1]
    for p in percentage:
        model = transferModel(model1, p)
        save_model(model, os.path.join(args.o, str(args.model_type) +'_'+str(p)+'.h5'))

