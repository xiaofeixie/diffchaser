#!/usr/bin/env python2.7
import argparse
import pickle
import os
import sys
import random

from keras.datasets import mnist

from keras.models import load_model
import numpy as np
from keras.datasets import cifar10
import glob
sys.path.append('../')


def color_preprocessing(x_test):
    x_test = x_test.astype('float32')
    mean = [125.307, 122.95, 113.865]
    std = [62.9932, 62.0887, 66.7048]
    for i in range(3):
        x_test[:, :, :, i] = (x_test[:, :, :, i] - mean[i]) / std[i]
    return x_test
def preprocessing_test_batch(x_test):
    x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)
    x_test = x_test.astype('float32')
    x_test /= 255
    return x_test

def createBatch(x_batch, batch_size, output_path, prefix):
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    batch_num = len(x_batch) / batch_size
    batches = np.split(x_batch, batch_num, axis=0)
    for i, batch in enumerate(batches):
        test = np.append(batch, batch, axis=0)
        saved_name = prefix + str(i) + '.npy'
        np.save(os.path.join(output_path, saved_name), test)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='control experiment')


    parser.add_argument('-model_type', help='Model type', choices=['mnist', 'vgg16', 'imagenet'], default='vgg16')
    parser.add_argument('-model_dir',help='')
    parser.add_argument('-batch_size', type=int, help='Number of images in one batch', default=1)

    parser.add_argument('-batch_num', type=int, help='Number of batches', default=30)
    args = parser.parse_args()
    if args.model_type == 'mnist':
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        batch = preprocessing_test_batch(x_test)
        x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)
    elif args.model_type == 'vgg16':
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        batch = color_preprocessing(x_test)
    else:
        assert (False)

    num_in_each_class = int((args.batch_size * args.batch_num) / 10)
    os.chdir(args.model_dir)
    # good = set()
    new_label = np.reshape(y_test, (10000,))
    good =  np.arange(10000)

    filenames = []
    for filename in glob.glob(args.model_dir+'/**/*.h5', recursive=True):
        filenames.append(filename)


    for filename in filenames:
        model = load_model(filename)
        result = np.argmax(model.predict(batch), axis=1)
        idx_good = np.where(new_label == result)[0]
        print(len(idx_good))
        good = np.intersect1d(good, idx_good)

    for cl in range(10):
        cl_indexes  = [i for i in good if new_label[i] == cl]
        selected = random.sample(cl_indexes, num_in_each_class)
        createBatch(x_test[selected], args.batch_size, 'seeds', str(cl)+'_')
    print('finish')
