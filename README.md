DiffChaser: Detecting Disagreements for Deep Neural Networks
======

This repository contains the framework, `DiffChaser`, for testing deep neural networks.

## Installation

```
pip install -r requirements.txt
```


## Example of Runing DiffChaser:

### Train a model
```
mkdir models
python model/LeNet5.py
```

### Generate a model with Quantitation

```
python utils/quantize.py  -i models/lenet5.h5 -o models
```

### Generate initial seeds 

```
python utils/ConstructSeeds.py -model_type mnist  -model_dir models -batch_size 1 -batch_num 30
```


### Run DiffChaser
```
python DiffTesting.py -i models/seeds/ -o output -type mnist -model_type lenet5 -model1 models/lenet5.h5 -model2 models/lenet5_0.01.h5
```