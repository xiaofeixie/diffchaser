import sys
sys.path.append('.')
from GeneticAlgorithm import Population
from PIL import Image
from keras.applications.vgg16 import preprocess_input
import random
import time
import argparse
import numpy as np
import os
import shutil

from keras.models import load_model

from keras import backend as K
from keras.utils.generic_utils import CustomObjectScope

import keras
from ImgMutators import Mutators
from model.BuildModel import build_model
def imagenet_preprocessing(input_img_data):
    temp = np.copy(input_img_data)
    temp = np.float32(temp)
    qq = preprocess_input(temp)  # final input shape = (1,224,224,3)
    return qq

def mnist_preprocessing(x_test):
    temp = np.copy(x_test)
    temp = temp.reshape(temp.shape[0], 28, 28, 1)
    temp = temp.astype('float32')
    temp /= 255
    return temp

def cifar_preprocessing(x_test):
    temp = np.copy(x_test)
    temp = temp.astype('float32')
    mean = [125.307, 122.95, 113.865]
    std = [62.9932, 62.0887, 66.7048]
    for i in range(3):
        temp[:, :, :, i] = (temp[:, :, :, i] - mean[i]) / std[i]
    return temp


shape_dic = {
    'vgg16': (32,32,3),
    'resnet20': (32,32,3),
    'lenet1': (28,28,1),
    'lenet4': (28,28,1),
    'lenet5': (28,28,1),
    'mobilenet': (224, 224,  3),
    'vgg19': (224, 224,  3),
    'resnet50': (224, 224,  3)
}
preprocess_dic = {
    'cifar10': cifar_preprocessing,
    'mnist': mnist_preprocessing,
    'imagenet': imagenet_preprocessing
}

def create_image_indvs(img, num):
    indivs = []
    indivs.append(img)
    for i in range(num-1):
        indivs.append(Mutators.mutate(img,img))
    return np.array(indivs)
def predict(input_data, model):
    inp = model.inputs
    #ensure that layers[-2] is the logit output
    logits_layer = model.layers[-2]
    sotfmax_layer = model.layers[-1]
    model = keras.Model(inp, [logits_layer.output, sotfmax_layer.output])
    outputs = model.predict(input_data)
    return outputs
def untarget_diff_object_func(model1, model2, preprocess, subtotal, diff_num, target_ratio=0.9):
    def func(indvs, ground_truth):
        # define target
        array = np.array(indvs)

        preprocessed = preprocess(array)

        outputs = predict(preprocessed, model1)
        quantize_outputs = predict(preprocessed, model2)

        results1 = np.argmax(outputs[-1], axis=1)
        results2 = np.argmax(quantize_outputs[-1], axis=1)

        is_diff = results1 != results2
        is_correct1 = results1 == ground_truth
        is_correct2 = results2 == ground_truth

        different = np.nonzero(is_diff == True)[0]
        correct1 = np.nonzero(is_correct1 == True)[0]
        correct2 = np.nonzero(is_correct2 == True)[0]

        index_result1 = np.intersect1d(different, correct1)
        index_result2 = np.intersect1d(different, correct2)

        index_result = np.union1d(index_result1, index_result2)


        #compute fitness

        logits_outputs = outputs[-2]
        logits_outputs2 = quantize_outputs[-2]

        fitness = []

        order_results = np.sort(logits_outputs, axis=1)
        order_results2 = np.sort(logits_outputs2, axis=1)


        #[0,subtotal] output1
        #[subtotal+1, 2*subtotal] output2
        #[2*subtotal+1, ..] others

        total_len = len(logits_outputs)
        prob_results1 = outputs[-1]
        prob_results2 = quantize_outputs[-1]



        d = diff_num * -1



        for i in range(subtotal):
            output = order_results[i]
            m = abs(output[d]-output[-1])*-1
            fitness.append(m)
        for i in range(subtotal, subtotal*2):
            output2 = order_results2[i]
            m = abs(output2[d]-output2[-1])*-1
            fitness.append(m)
        for i in range(2*subtotal,total_len):
            id1 = results1[i]
            id2 = results2[i]
            output = prob_results1[i]
            output2 = prob_results2[i]
            m = max(abs(output[id1]-output2[id1]),abs(output[id2]-output2[id2]))
            fitness.append(m)


        new_index_result = []
        for i in index_result:
            if prob_results1[i][results1[i]] > target_ratio and prob_results2[i][results2[i]]>target_ratio:
                new_index_result.append(i)


        return results1[new_index_result], results2[new_index_result], array[new_index_result],  new_index_result, fitness
    return func


def target_diff_object_func(model1, model2, preprocess, subtotal, target, target_ratio=0.9):
    def func(indvs, ground_truth):
        # define target
        array = np.array(indvs)

        # ind = np.reshape(array, (len(indvs),)+data_shape)
        preprocessed = preprocess(array)

        outputs = predict(preprocessed, model1)
        quantize_outputs = predict(preprocessed, model2)

        results1 = np.argmax(outputs[-1], axis=1)
        results2 = np.argmax(quantize_outputs[-1], axis=1)

        # is_diff = results1 != results2
        is_correct1 = results1 == ground_truth
        is_correct2 = results2 == ground_truth

        is_target1 = results1 == target
        is_target2 = results2 == target

        # different = np.nonzero(is_diff == True)[0]
        correct1 = np.nonzero(is_correct1 == True)[0]
        correct2 = np.nonzero(is_correct2 == True)[0]
        target1 = np.nonzero(is_target1 == True)[0]
        target2 = np.nonzero(is_target2 == True)[0]

        # index_result1 = np.intersect1d(different, correct1)
        index_result1 = np.intersect1d(correct1, target2)

        index_result2 = np.intersect1d(correct2, target1)

        index_result = np.union1d(index_result1, index_result2)


        logits_outputs = outputs[-2]
        logits_outputs2 = quantize_outputs[-2]

        fitness = []






        total_len = len(logits_outputs)
        prob_results1 = outputs[-1]
        prob_results2 = quantize_outputs[-1]




        for i in range(subtotal):
            output = logits_outputs[i]
            m = abs(np.max(output) - output[target])*-1
            fitness.append(m)
        for i in range(subtotal, subtotal*2):
            output2 = logits_outputs2[i]
            m = abs(np.max(output2) - output2[target])*-1
            # m = abs(output2[d]-output2[-1])*-1
            fitness.append(m)
        for i in range(2*subtotal,total_len):
            id1 = results1[i]
            id2 = results2[i]
            output = prob_results1[i]
            output2 = prob_results2[i]
            m = max(abs(output[id1]-output2[id1]),abs(output[id2]-output2[id2]))
            fitness.append(m)


        new_index_result = []
        for i in index_result:
            if prob_results1[i][results1[i]] > target_ratio and prob_results2[i][results2[i]]>target_ratio:
                new_index_result.append(i)


        return results1[new_index_result], results2[new_index_result], array[new_index_result], new_index_result, fitness
    return func

def build_mutate_func(ref_img):
    def func(indv):
        return Mutators.mutate(indv, ref_img)
    return func


def build_save_func(npy_output, img_output, ground_truth, seedname, istarget):
    def save_func(indvs, round):
        results1, results2, data, indexes = indvs
        for i, item in enumerate(results1):
            name = seedname+ '_'+'_'+ istarget+'_' + str(ground_truth) +'_' + str(results1[i]) + '_' + str(results2[i]) + '_'+ str(time.time()) +'_'+ str(round)
            np.save(os.path.join(npy_output, name + '.npy'), data[i])
            x = np.uint8(data[i])
            shape = x.shape
            if shape[-1] == 1:
                x = np.reshape(x, (shape[0], shape[1]))
            img0 = Image.fromarray(x)
            img0.save(os.path.join(img_output, name + '.png'))
    return  save_func

if __name__ == '__main__':

    start_time = time.time()
    random.seed(time.time())

    random.seed(time.time())

    parser = argparse.ArgumentParser(description='coverage guided fuzzing')

    parser.add_argument('-i', help='input seed dir')
    parser.add_argument('-o', help='seed output')
    parser.add_argument('-pop_num', help='seed output', type =int, default=700)
    parser.add_argument('-type', help="target model fuzz", choices=['mnist','imagenet','cifar10'],
                        default='mnist')
    parser.add_argument('-model_type', help='Out path',
                        choices=['lenet1', 'lenet5', 'resnet20', 'mobilenet', 'vgg16', 'resnet50'], default='resnet20')

    parser.add_argument('-model1', help="fuzzer for quantize")
    parser.add_argument('-model2', help="fuzzer for quantize")
    parser.add_argument('-ratio', type=float,help="fuzzer for quantize", default=0)
    parser.add_argument('-subtotal', type=int, default=300)

    parser.add_argument('-timeout', help="threshold for determining neuron activated", type=int, default=1000000000)
    parser.add_argument('-max_iteration', help="threshold for determining neuron activated", type=int, default=10000000000)



    parser.add_argument('-first_attack', choices=[0,1], type=int, default=1)

    parser.add_argument('-target', choices=[0,1], type=int, default=0)
    parser.add_argument('-uncertainty_degree',  choices=[2,3,4,5,6,7,8,9], default=2, type=int)
    args = parser.parse_args()

    #
    if os.path.exists(args.o):
        shutil.rmtree(args.o)

    img_dir =  os.path.join(args.o, 'imgs')
    crash_dir = os.path.join(args.o, 'crashes')


    os.makedirs(crash_dir)
    os.makedirs(img_dir)

    plot_file = open(os.path.join(args.o, 'plot.log'), 'a+')
    preprocess = preprocess_dic[args.type]
    # data_shape = shape_dic[args.model]
    if args.type == 'mobilenet' or args.type == 'resnet50':

        with CustomObjectScope({'relu6': keras.applications.mobilenet.relu6,
                                'DepthwiseConv2D': keras.applications.mobilenet.DepthwiseConv2D}):
            model = load_model(args.model1)
            quantize_model = load_model(args.model2)

    else:
        model = build_model(weights_path=args.model1, type=args.model_type)
        quantize_model = build_model(weights_path=args.model2, type=args.model_type)


    inputs = os.listdir(args.i)


    index = 0


    # data = np.load(args.i)
    # x_test, y_test = data['x_test'],data['y_test']
    # for i  in range(len(x_test)):
    inputs = os.listdir(args.i)
    first = time.time()
    for i, seed in enumerate(inputs):
            print('Start test %d-th img, success rate %f'%(i, float(index)/i if i >0 else 1))
            plot_file.write('%s,%d' % (seed, time.time()))
            seed_pth = os.path.join(args.i, seed)
            data = np.load(seed_pth)
            img =  data[1]
            ground_truth = np.argmax(model.predict(preprocess(data[1:2])),axis=1)[0]

            inds = create_image_indvs(img, args.pop_num)

            mutation_function = build_mutate_func(img)



            if args.target == 0:
                save_function = build_save_func(crash_dir, img_dir, ground_truth, seed, 'n')
                fitness_compute_function = untarget_diff_object_func(model, quantize_model, preprocess,
                                                                     diff_num=args.uncertainty_degree,
                                                                     subtotal=args.subtotal, target_ratio=args.ratio)
                pop = Population(inds,mutation_function,fitness_compute_function,save_function,ground_truth,
                                 first_attack=args.first_attack,
                                 subtotal=args.subtotal, max_time=args.timeout, seed=img,max_iteration=args.max_iteration)
                plot_file.write(',%d,%d\n'%(pop.first_time_used,pop.success))
                index += pop.success
            else:
                save_function = build_save_func(crash_dir, img_dir, ground_truth, seed, 't')
                plot_file.write(':\n')

                found = 0

                for target in range(10):
                    if target == ground_truth:
                        continue
                    fitness_compute_function = target_diff_object_func(model, quantize_model, preprocess,
                                                                   subtotal=args.subtotal,
                                                                       target=target,
                                                                   target_ratio=args.ratio)
                    pop = Population(inds, mutation_function, fitness_compute_function, save_function, ground_truth,first_attack=args.first_attack,
                                     subtotal=args.subtotal, max_time=args.timeout, seed=img, max_iteration=args.max_iteration)
                    plot_file.write(' %d,%d,%d,%d\n' % (target, pop.first_time_used, pop.first_iteration_used, pop.success))

                    found = 1 if pop.success == 1 else found

                index += found


            plot_file.flush()
    plot_file.write('Finish: %d, %d/%d'%(time.time()-first,index,len(inputs)))
    plot_file.close()









