'''
LeNet-5
'''

# usage: python MNISTModel3.py - train the model

from __future__ import print_function

from keras.layers.normalization import BatchNormalization
from keras.layers import Conv2D, Dense, Input, add, Activation, GlobalAveragePooling2D
from keras import optimizers, regularizers
from keras.applications.vgg16 import VGG16
from keras.layers import Conv2D

from keras.layers import Convolution2D, MaxPooling2D, Input, Dense, Activation, Flatten
from keras.models import Model
from model.LeNet1 import LeNet1
from model.LeNet4 import LeNet4
from model.LeNet5 import LeNet5
import argparse
def build_model(weights_path, type='lenet5'):
    mnist_tensor = Input(shape=(28, 28, 1))
    cifar_tensor = Input(shape=(32, 32, 3))
    if type == 'lenet1':
        return LeNet1(input_tensor=mnist_tensor, train=False,  path=weights_path)
    elif type == 'lenet4':
        return LeNet4(input_tensor=mnist_tensor, train=False,  path=weights_path)
    elif type == 'lenet5':
        return LeNet5(input_tensor=mnist_tensor, train=False,  path=weights_path)
    elif  type == 'resnet20':
        weight_decay = 0.0001
        stack_n = 3


        def residual_block(intput, out_channel, increase=False):
            if increase:
                stride = (2, 2)
            else:
                stride = (1, 1)

            pre_bn = BatchNormalization()(intput)
            pre_relu = Activation('relu')(pre_bn)

            conv_1 = Conv2D(out_channel, kernel_size=(3, 3), strides=stride, padding='same',
                            kernel_initializer="he_normal",
                            kernel_regularizer=regularizers.l2(weight_decay))(pre_relu)
            bn_1 = BatchNormalization()(conv_1)
            relu1 = Activation('relu')(bn_1)
            conv_2 = Conv2D(out_channel, kernel_size=(3, 3), strides=(1, 1), padding='same',
                            kernel_initializer="he_normal",
                            kernel_regularizer=regularizers.l2(weight_decay))(relu1)
            if increase:
                projection = Conv2D(out_channel,
                                    kernel_size=(1, 1),
                                    strides=(2, 2),
                                    padding='same',
                                    kernel_initializer="he_normal",
                                    kernel_regularizer=regularizers.l2(weight_decay))(intput)
                block = add([conv_2, projection])
            else:
                block = add([intput, conv_2])
            return block

            # build model
            # total layers = stack_n * 3 * 2 + 2
            # stack_n = 5 by default, total layers = 32, which is resnet32
            # input: 32x32x3 output: 32x32x16

        x = Conv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same',
                   kernel_initializer="he_normal",
                   kernel_regularizer=regularizers.l2(weight_decay))(cifar_tensor)

        # input: 32x32x16 output: 32x32x16
        for _ in range(stack_n):
            x = residual_block(x, 16, False)

        # input: 32x32x16 output: 16x16x32
        x = residual_block(x, 32, True)
        for _ in range(1, stack_n):
            x = residual_block(x, 32, False)

        # input: 16x16x32 output: 8x8x64
        x = residual_block(x, 64, True)
        for _ in range(1, stack_n):
            x = residual_block(x, 64, False)

        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        x = GlobalAveragePooling2D()(x)

        # input: 64 output: 10
        x = Dense(10, name='before_softmax',
                  kernel_initializer="he_normal",
                  kernel_regularizer=regularizers.l2(weight_decay))(x)
        x = Activation('softmax')(x)
    elif type == 'vgg16':
        model = VGG16(input_tensor=cifar_tensor)
        model.load_weights(weights_path)
        return model

    else:
        assert (False)
    model = Model(cifar_tensor, x)
    model.load_weights(weights_path)
    return model

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='coverage guided fuzzing')
    parser.add_argument('-i', help='input seed dir')
    parser.add_argument('-o', help='seed output')
    parser.add_argument('-type', help='seed output')
    args = parser.parse_args()

    if args.type == 'lenet1' or args.type == 'lenet5':
        input_shape = (28, 28, 1)
    elif args.type == 'resnet20' or args.type == 'vgg16':
        input_shape = (32, 32, 3)
    else:
        assert (False)
    input_tensor = Input(shape=input_shape)
    model = build_model(args.i,input_tensor=input_tensor, type=args.type)
    model.save(args.o)
