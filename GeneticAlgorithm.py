import sys
sys.path.append('.')
import numpy as np
import copy
import random
import time
from ImgMutators import Mutators
# from DiffTesting2 import create_image_indvs
def create_image_indvs(img, num):
    indivs = []
    indivs.append(img)
    for i in range(num-1):
        indivs.append(Mutators.mutate(img,img))
    return np.array(indivs)
class Population():

    def __init__(self, individuals,
                 mutation_function,
                 fitness_compute_function,
                 save_function,
                 groud_truth,
                 subtotal,
                 first_attack,
                 seed,
                 max_iteration,
                 tour_size=20, cross_rate=0.5, mutate_rate=0.01, max_trials = 50, max_time=30):
        self.ground_truth = groud_truth
        self.cross_rate = cross_rate
        self.mutate_rate = mutate_rate
        self.seed =seed
        self.individuals = individuals # a list of individuals, current is numpy
        self.tournament_size = tour_size
        self.fitness = None   # a list of fitness values
        self.pop_size = len(self.individuals)
        self.subtotal = subtotal

        self.firstattack = first_attack



        self.mutation_func = mutation_function
        self.fitness_fuc = fitness_compute_function
        self.save_function = save_function
        self.order = []
        self.best_fitness = -1000
        self.success = 0



        # self.total_bugs = 0
        # self.total_diversity = set()

        self.first_time_used = max_time
        self.first_iteration_used = max_iteration

        # for i in range(max_trials):
        start_time = time.time()
        i = 0
        while True:

            if i > max_iteration:
                break
            if time.time()-start_time > max_time:
                break
            i += 1
            results = self.evolvePopulation()
            if results is None:
                print("   Total generation: %d, best fitness:%.9f"%(i, self.best_fitness))
            else:
                self.save_function(results,i)
                self.success = 1

                # results1[new_index_result], results2[new_index_result], array[new_index_result], r_indexes


                if self.first_time_used == max_time:
                    self.first_time_used = time.time()-start_time
                    self.first_iteration_used = i
                if self.firstattack == 1:
                    break
                else:
                    r_indexes = results[-1]
                    new_invs = create_image_indvs(self.seed, len(r_indexes))
                    for j in range(len(new_invs)):
                        self.individuals[r_indexes[j]] = new_invs[j]



    def crossover(self, ind1, ind2):
        shape = ind1.shape
        ind1 = ind1.flatten()
        ind2 = ind2.flatten()
        new_ind = np.copy(ind1)

        for i in range(len(ind1)):
            if random.uniform(0, 1) < self.cross_rate:
                new_ind[i] = ind1[i]
            else:
                new_ind[i] = ind2[i]
        return np.reshape(new_ind,shape)

    def evolvePopulation(self):

        results = self.fitness_fuc(self.individuals, self.ground_truth)

        objects = results[-2]
        self.fitness = results[-1]

        if len(objects) > 0:
            return results[:-1]





        """
            sorted_fitness_indexes: the ordered indexes based on fitness value
            sorted_fitness_indexes[0] is the index of individual with the best fitness
        """
        sorted_fitness_indexes1 = sorted(range(0,self.subtotal), key=lambda k: self.fitness[k], reverse=True)
        sorted_fitness_indexes2 = sorted(range(self.subtotal, self.subtotal*2), key=lambda k: self.fitness[k], reverse=True)
        sorted_fitness_indexes3 = sorted(range(self.subtotal*2, len(self.fitness)), key=lambda k: self.fitness[k], reverse=True)
        """
            tournaments: randomly select a tournament from the individuals and get the indv with best fittness
            Instead of select from individuals , we select from the sorted indexes (i.e., sorted_fitness_indexes) randomly.
            sorted_fitness_indexes[order_seq1[0]] is the index of indivitual with best fitness in the selected tournament.
        """

        new_indvs = []


        sorted_fitnesses = [sorted_fitness_indexes1, sorted_fitness_indexes2, sorted_fitness_indexes3]
        temp = int(self.subtotal*2 + (self.pop_size-self.subtotal*2)/2)
        ranges = [(0,self.subtotal), (self.subtotal,self.subtotal*2),(self.subtotal*2, temp)]
        tour_ranges = [(0, self.subtotal), (0, self.subtotal), (0, self.pop_size-self.subtotal*2)]

        for j in range(len(sorted_fitnesses)):
            sorted_fitness_indexes = sorted_fitnesses[j]
            best_index = sorted_fitness_indexes[0]
            (start,end) = ranges[j]
            (tour_start,tour_end) = tour_ranges[j]
            for i in range(start,end):
                item = self.individuals[i]
                if i == best_index:  # keep best
                    new_indvs.append(item)
                else:
                    # print(tour_start,tour_end,'-------')
                    order_seq1 = np.sort(np.random.choice(np.arange(tour_start,tour_end), self.tournament_size, replace=False))
                    order_seq2 = np.sort(np.random.choice(np.arange(tour_start,tour_end), self.tournament_size, replace=False))

                    # Finally, we get two best individual in the two tournaments.
                    # print(order_seq1[0],'!')
                    # print(sorted_fitness_indexes[order_seq1[0]],'--')
                    first_individual = self.individuals[sorted_fitness_indexes[order_seq1[0]]]
                    second_individual = self.individuals[
                        sorted_fitness_indexes[order_seq2[0] if order_seq2[0] != order_seq1[0] else order_seq2[1]]]

                    # Cross over
                    ind = self.crossover(first_individual, second_individual)

                    if random.uniform(0, 1) < self.mutate_rate:
                        ind = self.mutation_func(ind)
                    new_indvs.append(ind)

        for i in range(temp, self.pop_size):

            if i == sorted_fitness_indexes3[0]:  # keep best
                new_indvs.append(item)
            else:
                iv1 = random.randint(0, self.subtotal-1)
                iv2 = random.randint(self.subtotal,self.subtotal*2-1)


                # Finally, we get two best individual in the two tournaments.
                first_individual = self.individuals[iv1]
                second_individual = self.individuals[iv2]

                # Cross over
                ind = self.crossover(first_individual, second_individual)

                if random.uniform(0, 1) < self.mutate_rate:
                    ind = self.mutation_func(ind)
                new_indvs.append(ind)



        self.individuals = new_indvs
        self.best_fitness = max(self.fitness[sorted_fitness_indexes1[0]], self.fitness[sorted_fitness_indexes2[0]])
        return None

    def evolvePopulation_bac(self):

        results = self.fitness_fuc(self.individuals, self.ground_truth)

        objects = results[-2]
        self.fitness =results[-1]

        if len(objects) > 0:
            return results[:-1]
        """
            sorted_fitness_indexes: the ordered indexes based on fitness value
            sorted_fitness_indexes[0] is the index of individual with the best fitness 
        """
        sorted_fitness_indexes = sorted(range(len(self.fitness)), key=lambda k: self.fitness[k], reverse=True)

        """
            tournaments: randomly select a tournament from the individuals and get the indv with best fittness 
            Instead of select from individuals , we select from the sorted indexes (i.e., sorted_fitness_indexes) randomly.
            sorted_fitness_indexes[order_seq1[0]] is the index of indivitual with best fitness in the selected tournament. 
        """

        new_indvs = []
        best_index = sorted_fitness_indexes[0]
        for i, item in enumerate(self.individuals):
            if i == best_index:  # keep best
                new_indvs.append(item)
            else:
                order_seq1 = np.sort(np.random.choice(np.arange(self.pop_size ), self.tournament_size, replace=False))
                order_seq2 = np.sort(np.random.choice(np.arange(self.pop_size ), self.tournament_size, replace=False))

                # Finally, we get two best individual in the two tournaments.
                first_individual = self.individuals[sorted_fitness_indexes[order_seq1[0]]]
                second_individual = self.individuals[sorted_fitness_indexes[order_seq2[0] if order_seq2[0] != order_seq1[0] else order_seq2[1]]]

                # Cross over
                ind = self.crossover(first_individual, second_individual)

                if random.uniform(0, 1) < self.mutate_rate:
                    ind = self.mutation_func(ind)
                new_indvs.append(ind)
        self.individuals = new_indvs
        self.best_fitness = self.fitness[best_index]
        return None
